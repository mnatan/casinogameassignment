/**
 *
 * Created by natan on 28.04.16.
 */


// Module Pattern - I have concerns regarding testability
var CasinoGame = (function () {

// Shameful globals section
    var sizesHash = {
        originalSize: {
            x: 960,
            y: 536
        },
        strips: [
            {xFrom: 70, xTo: 300},
            {xFrom: 310, xTo: 540},
            {xFrom: 550, xTo: 785}
        ],
        fontSize: 50,
        elements: {
            playButton: {
                xFrom: 815,
                yFrom: 210,
                xTo: 930,
                yTo: 330
            },
            symbol: {
                xFrom: 320,
                yFrom: 200,
                xTo: 320 + 215,
                yTo: 200 + 140
            },
            symbolSelect: {
                xFrom: 790,
                yFrom: 50,
                xTo: 950,
                yTo: 50 + 60
            },
            bigInfo: {
                xFrom: 150,
                yFrom: 200,
                xTo: 800,
                yTo: 200 + 150
            }
        }
    };
    var languages = {
        messages: {
            win: {
                en: "You win!",
                pl: "Wygrana!"
            },
            loose: {
                en: "Sorry, try again.",
                pl: "Porazka, sprobuj jeszcze raz."
            },
            placeBet: {
                en: "You must place a bet.",
                pl: "Musisz obstawic symbol."
            },
            betHere: {
                en: "Bet here",
                pl: "Obstaw"
            }
        },
        symbols: {
            WILD: {
                en: "WILD",
                pl: "WILD"
            },
            WATERMELON: {
                en: "WATERMELON",
                pl: "ARBUZ"
            },
            STRAWBERRY: {
                en: "STRAWBERRY",
                pl: "TRUSKAWKA"
            },
            PINEAPPLE: {
                en: "PINEAPPLE",
                pl: "ANANAS"
            },
            LEMON: {
                en: "LEMON",
                pl: "CYTRYNA"
            },
            GRAPES: {
                en: "GRAPES",
                pl: "WINOGRONA"
            },
        }
    };
    var currentSize = {};
    var symbolsDefinitions = {};
    var currentSymbol = "";
    var shrinkTime = 200;

// Initializations and loading
    function initScreen() { // called on resize
        preventScrolling();
        setCurrentLanguage();


        currentSize = {
            x: (window.innerWidth > 0) ? window.innerWidth : screen.width,
            y: (window.innerHeight > 0) ? window.innerHeight : screen.height
        };

        // position and scale elements
        ["playButton", "symbolSelect", "symbol", "bigInfo"].forEach(function (key) {
            positionElement(key);
        });
    }

    function loadData() {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState == 4 && xhttp.status == 200) {
                var symbols = parseConfig(xhttp);
                symbolsDefinitions = symbols;
                currentSymbol = getRandomSymbol(symbols);
                updateSymbolsSelect(symbols);
                enablePlayButton();
                drawSymbol();
            }
        };
        xhttp.open("GET", "config.xml", true);
        xhttp.send();
    }

    function calculatePercentage(sizesHash) {

        // strips
        for (var i = 0; i < sizesHash["strips"].length; i++) {
            ["xFrom", "xTo"].forEach(function (j) {
                sizesHash["strips"][i][j] /= sizesHash["originalSize"]["x"];
            });
        }

        // elements
        Object.keys(sizesHash["elements"]).forEach(function (el) {
            ["x", "y"].forEach(function (j) {
                sizesHash["elements"][el][j + "From"] /= sizesHash["originalSize"][j];
                sizesHash["elements"][el][j + "To"] /= sizesHash["originalSize"][j];
            });
        });

        return sizesHash;
    }

    function parseConfig(xml) {

        var xmlDoc = xml.responseXML;
        var imgPath = removeWhitespace(
            xmlDoc
                .getElementsByTagName("path")[0]
                .textContent
        );

        var symbols = xmlDoc
            .getElementsByTagName("symbol");

        var symbolsHash = {};
        for (var i = 0; i < symbols.length; i++) {
            var name = removeWhitespace(
                symbols[i].getElementsByTagName("name")[0].childNodes[0].nodeValue
            );
            var file = imgPath + removeWhitespace(
                    symbols[i].getElementsByTagName("file")[0].childNodes[0].nodeValue
                );

            symbolsHash[name] = file;
        }

        return symbolsHash;

    }

// Drawing and animations
    function positionElement(key) {
        var element = document.getElementById(key);
        var elementSize = sizesHash["elements"][key];

        element.style.position = "fixed";
        element.style.left = elementSize["xFrom"] * 100 + "%";
        element.style.top = elementSize["yFrom"] * 100 + "%";
        element.style.width = currentSize["x"] * (elementSize["xTo"] - elementSize["xFrom"]) + "px";
        element.style.height = currentSize["y"] * (elementSize["yTo"] - elementSize["yFrom"]) + "px";

    }

    function drawSymbol() {
        var symbolEl = document.getElementById("symbol");

        positionElement("symbol");

        symbolEl.style.backgroundImage = "url('" + symbolsDefinitions[currentSymbol] + "')";
        symbolEl.className = "";
    }

    function animateNextSymbol(symbol) {
        var symbolEl = document.getElementById("symbol");
        symbolEl.style.transition = shrinkTime / 1000 + "s";
        symbolEl.className = "shrink";
        setTimeout(drawSymbol, shrinkTime);
    }

    function displayInfo(info, color) {
        var biginfo = document.getElementById("bigInfo");
        positionElement("bigInfo");
        biginfo.innerHTML = info[languages.current];
        biginfo.style.color = color;
        biginfo.className = "visible";
        biginfo.style.fontSize = sizesHash["fontSize"] * (currentSize["y"] / sizesHash["originalSize"]["y"] ) + "px";
        setTimeout(function () {
            biginfo.className = "";
            biginfo.style.fontSize = sizesHash["fontSize"] * (currentSize["y"] / sizesHash["originalSize"]["y"] ) + 10 + "px";
        }, 3000);
    }

// System logic
    function generateNextSymbol() {
        var nextSymbol = getRandomSymbol(symbolsDefinitions);
        currentSymbol = nextSymbol;
        animateNextSymbol(nextSymbol);
    }

    function updateSymbolsSelect(symbolsHash) {

        var content = "<option value='' disabled selected>" + languages.messages.betHere[languages.current] + "</option>";

        Object.keys(symbolsHash)
            .forEach(function (key) {
                content += "<option value='" + key + "'>" + languages.symbols[key][languages.current] + "</option>";
                preloadSymbols(symbolsHash[key]);
            });

        document.getElementById("symbolSelect").innerHTML = content;

    }

    function playClicked() {
        var select = document.getElementById("symbolSelect");
        var selected = select.options[select.selectedIndex].value;
        if (selected != "") {
            disableControls(selected);

            var spins = 5 + Math.floor(Math.random() * 5);
            for (var i = 0; i < spins; i++) {
                setTimeout(generateNextSymbol, i * shrinkTime * 2);
            }

            setTimeout(checkOutcome.bind(null, selected), spins * shrinkTime * 2);
            setTimeout(enableControls, spins * shrinkTime * 2 + 3000);
        } else {
            displayInfo(languages.messages.placeBet, "white");
            enableControls();
        }
    }

    function checkOutcome(selected) {
        if (selected == currentSymbol) {
            displayInfo(languages.messages.win, "lawngreen");
        } else {
            displayInfo(languages.messages.loose, "white");
        }
    }

// Helper functions
    function removeWhitespace(str) {
        return str.replace(/^\s*(\S*)\s*$/, '$1');
    }

    function preventScrolling() {
        // preventing touchstart was breaking <select>.

        // document.body.addEventListener('touchstart', function (e) {
        //     e.preventDefault();
        // });
        document.body.addEventListener('touchmove', function (e) {
            e.preventDefault();
        });
    }

    function preloadSymbols() {
        var images = [];
        for (i = 0; i < preloadSymbols.arguments.length; i++) {
            images[i] = new Image();
            images[i].src = preloadSymbols.arguments[i];
        }
    }

    function enableControls() {
        updateSymbolsSelect(symbolsDefinitions);
        enablePlayButton();
    }

    function disableControls(selected) {
        document.getElementById("symbolSelect").innerHTML =
            "<option value='" + selected + " seleted disbled'>" + languages.symbols[selected][languages.current] + "</option>";
        var play = document.getElementById("playButton");
        play.src = "img/BTN_Spin_d.png";
        play.onclick = null;
    }

    function enablePlayButton() {
        var play = document.getElementById("playButton");
        play.src = "img/BTN_Spin.png";
        play.onclick = playClicked;
    }

    function getRandomSymbol(symbolsHash) {
        var keys = Object.keys(symbolsHash);
        return keys[Math.floor(Math.random() * keys.length)];
    }

    function parseUriParams(val) {
        var result;
        var tmp = [];
        location.search
            //.replace ( "?", "" ) 
            // this is better, there might be a question mark inside
            .substr(1)
            .split("&")
            .forEach(function (item) {
                tmp = item.split("=");
                if (tmp[0] === val) result = decodeURIComponent(tmp[1]);
            });
        return result;
    }

    function setCurrentLanguage() {
        var param = parseUriParams("lan") || "en";
        param = (param == "undefined") ? "en" : param;
        // FIXME wrong param
        languages.current = param;
    }

    window.onload = function () {
        sizesHash = calculatePercentage(sizesHash);
        initScreen();
        loadData();
    };

    return {refresh: initScreen}

})();
